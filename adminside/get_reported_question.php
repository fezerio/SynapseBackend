<?php
include_once 'dbconnect.php';

$sql="SELECT rq.id, rq.question_id,rq.user_id, q.chapter_id,q.question,q.chapter_id,q.question,q.option_a,q.option_b,q.option_c,q.option_d,q.answer,q.explanation,c.name as chapter_name, s.name as subject_name, (Select Count(*) from reported_question d where d.question_id=rq.question_id) as no_of_reports from reported_question rq INNER JOIN question q ON rq.question_id=q.id INNER JOIN chapter c ON c.id=q.chapter_id INNER JOIN subject s ON c.subject_id = s.id Group By question_id";
$r=mysqli_query($dbsel,$sql);
$data=array();
while($row=mysqli_fetch_assoc($r))
	$data[]=$row;
echo json_encode($data);