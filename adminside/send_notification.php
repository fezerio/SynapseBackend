<?php
class NotificationService
{
    public $notificationbody;
    public $notificationTitle;

    function __construct($notificationbody, $notificationTitle)
    {
        $this->notificationbody = $notificationbody;
        $this->notificationTitle = $notificationTitle;
    }


    function sendNotification()
    {
        if ($this->notificationbody == null || $this->notificationTitle == null) {
            throw new Exception('Provide Notification title and body');
        }
        define('API_ACCESS_KEY', 'AAAARI--Tf0:APA91bFksqcJ0k5tNNr1LdJXUmNxLlOtVc1ClivhFbZRJ-jQaIWkpf5DRolqz6m0Hc-i-tsbUIhJbTwpHfjqdW9HoZiADa9WnX8l1VlcO49wcDmzLSWQGIXMB0uK3kb_n9aYN3BpLglS');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $topic = '/topics/users';

        $notification = [
            'title' => $this->notificationTitle,
            'body' => $this->notificationbody,
        ];
        $extraNotificationData = ["message" => $notification];

        $fcmNotification = [
            'notification' => $notification,
            'to' => $topic,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
