<?php
include_once 'dbconnect.php';
//0,0 for daily test
//-1,-1 for mock test
$topicId=0;
$subjectId=0;

$sql="SELECT user.name name,user.photo_url photo_url, user_id, COUNT(*) total,SUM(CASE WHEN qt.correct_answer = qt.user_answer THEN 1 ELSE 0 END) AS right_answer,
      SUM(CASE WHEN qt.correct_answer != qt.user_answer THEN 1 ELSE 0 END) AS wrong_answer from question_tracking qt inner JOIN user ON qt.user_id=user.id WHERE qt.chapter_id IN (SELECT id from chapter where topic_id='{$topicId}' AND subject_id='{$subjectId}') GROUP BY qt.user_id order by right_answer desc" ;
$r=mysqli_query($dbsel,$sql);
$data=array();
while($row=mysqli_fetch_assoc($r))
	$data[]=$row;
echo json_encode($data); 