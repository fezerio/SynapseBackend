<?php
include_once 'dbconnect.php';

$date = new DateTime();
$currentDate = $date->format('Y-m-d');
$sql = "(SELECT * from chapter c WHERE c.topic_id=-1 && c.subject_id=-1 && day(live_date)=day('{$currentDate}')) UNION (SELECT * from chapter c2 WHERE c2.topic_id=-1 && c2.subject_id=-1 && ((day(live_date)-day('{$currentDate}'))<4) && ((day(live_date)-day('{$currentDate}'))>0) ORDER BY live_date asc LIMIT 2 )";
$r = mysqli_query($dbsel, $sql);
$data = array();

while ($row = mysqli_fetch_assoc($r)) {
	$data[] = $row;
}

echo json_encode($data);
