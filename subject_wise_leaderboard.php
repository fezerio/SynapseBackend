<?php
include_once 'dbconnect.php';

$sql="SELECT user.name name,chapter.name chapter_name, user_id, Count(*)total,SUM(CASE WHEN qt.correct_answer = qt.user_answer THEN 1 ELSE 0 END) AS right_answer,
      SUM(CASE WHEN qt.correct_answer != qt.user_answer THEN 1 ELSE 0 END) AS wrong_answer from question_tracking qt LEFT JOIN user ON qt.user_id=user.id   LEFT JOIN chapter on qt.chapter_id= chapter.id WHERE qt.chapter_id IN (SELECT id from chapter where topic_id NOT IN (0,1) AND subject_id NOT IN (0,1 )) GROUP BY qt.user_id order by right_answer desc" ;
$r=mysqli_query($dbsel,$sql);
$data=array();
while($row=mysqli_fetch_assoc($r))
	$data[]=$row;
echo json_encode($data); 